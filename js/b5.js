/**
 * Dữ liệu đầu vào là một số có 2 chữ số mà người dùng nhập
 *
 * Xử lý :
 * gán giá trị mà người dùng nhập number_user
 * tách chữ số hàng đơn vị : %10 rồi gán vào biến hangDonvi
 * tách chữ số hàng chục : Math.floor(/10) rồi gán vào biến hangChuc
 */

var number_user, hangDonvi, hangChuc, result;

function sum_number() {
  number_user = document.getElementById("user_number");
  hangDonvi = Number(number_user.value) % 10;
  hangChuc = Math.floor(Number(number_user.value) / 10);
  result = hangDonvi + hangChuc;
  document.getElementById("hang_don_vi").innerHTML =
    "Chữ số hàng đơn vị là " + hangDonvi;
  document.getElementById("hang_chuc").innerHTML =
    "Chữ số hàng chục là " + hangChuc;
  document.getElementById("tong").innerHTML = "Tổng các chữ số là " + result;
}
