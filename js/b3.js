/**
 * dữ liệu đầu vào là số tiền USD mà người dùng muốn quy đổi
 *
 * Xử lý :
 * lấy số tiền người dùng nhập và gán vào biến moneyUser
 * nhân * 23500 , gán vào biến result , rồi truyền giá trị vào ô input
 */

var moneyUser, result;
function change__money() {
  moneyUser = document.getElementById("money_usd");
  result = Number(moneyUser.value) * 23500;
  document.getElementById("money_vnd").value = result + " VND";
}
