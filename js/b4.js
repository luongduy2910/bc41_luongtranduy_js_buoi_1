/**
 * Dữ liệu đầu vào là chiều dài và chiều rộng của hình chữ nhật mà người dùng nhập
 *
 * Xử lý :
 * gán 2 giá trị chiều dài và chiều rộng vào 2 biến longsUser và widthUser
 * Áp dụng công thức tính chu vi và diện tích của hình chữ nhật
 *
 *
 */

var longsUser, widthUser, resultPerimeter, resultArea;
longsUser = document.getElementById("longs");
widthUser = document.getElementById("width");
function perimeter_user() {
  resultPerimeter = (Number(longsUser.value) + Number(widthUser.value)) * 2;
  document.getElementById("perimeter").innerHTML =
    "Chu vi hình chữ nhật là " + resultPerimeter;
}
function area_user() {
  resultArea = Number(longsUser.value) * Number(widthUser.value);
  document.getElementById("area").innerHTML =
    "Diện tích hình chữ nhật là " + resultArea;
}
