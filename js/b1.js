/**
 * dữ liệu vào là số ngày mà người dùng nhập vào
 *
 * xử lý :
 * -Gán số ngày của người dùng nhập vào biến date_work
 * -Nhân cho 100000 rồi gán vào biến result
 * -In ra kết quả cho người biết số tiền lương của họ
 */

var date_work, result;

function salary_user() {
  date_work = document.getElementById("date");
  result = Number(date_work.value) * 100000;
  result = new Intl.NumberFormat("vn-VN").format(result);
  document.getElementById("salary__month").innerHTML =
    "Tiền lương của bạn tháng này là : " + result;
}

