/**
 * dữ liệu đầu vào là 5 số thực người dùng nhập vào
 *
 * xử lý :
 * gán 5 số thực vào 5 biến khác nhau, sử dụng Math.sum() để tính tổng và chia cho 5
 */

var num1, num2, num3, num4, num5, averageNum;

function average_num() {
  num1 = document.getElementById("number1");
  num2 = document.getElementById("number2");
  num3 = document.getElementById("number3");
  num4 = document.getElementById("number4");
  num5 = document.getElementById("number5");
  averageNum =
    (Number(num1.value) +
      Number(num2.value) +
      Number(num3.value) +
      Number(num4.value) +
      Number(num5.value)) /
    5;
  document.getElementById("average-num").innerHTML =
    "Giá trị trung bình của 5 số bạn nhập là: " + averageNum;
}
